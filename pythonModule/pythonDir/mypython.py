from use_classifier import main
from os import sep as my_separator

def run():
    main(
         'pythonModule'+my_separator+
         'pythonDir'+my_separator+
         'cls_gen_classification_percentage_year_verification[2016]_154.cls4720596')
if __name__ == '__main__':
    run()