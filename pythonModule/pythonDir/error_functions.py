import collections
from typing import Tuple

import numpy as np

ErrorFunction = collections.namedtuple("ErrorFunction", ["functions", "parameters", "name", "order"])


def evaluate_error_function(column: np.ndarray, error_function: ErrorFunction) -> np.ndarray:
    ef = error_function
    fil = np.zeros(column.shape[0], dtype=np.bool)  # filter
    for f in ef.functions:
        fil = np.logical_or(fil, f(column))
    return fil.sum() / fil.shape[0]


def lt_value_err(a: np.ndarray, b: np.ndarray, limit: float):
    a = np.asarray(a)
    b = np.asarray(b)
    return np.abs(a - b) < limit


# def lt_percent_err(estimation: np.ndarray, value: np.ndarray, limit: float):
#     est = np.asarray(estimation)
#     val = np.asarray(value)
#     return np.logical_or(est > (1 - limit) * val, est < (1 + limit) * val)

def mse_err(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    return np.power(np.power(a - b, 2), 1).sum() / a.shape[0]


def lt_err(a: np.ndarray, b: np.ndarray, limit: float):
    if not len(a):
        return 0
    a = np.asarray(a)
    b = np.asarray(b)
    return (np.abs(a - b) < limit).sum() / a.shape[0]


def lt_log_err(a: np.ndarray, b: np.ndarray, par: Tuple[float, float]) -> float:
    """
    Return error ration on log scale.
    :param a: number vector
    :param b: number vector to compare
    :param par: tuple of limit and base of logarithm
    :return:
    """
    if not len(a):
        return 0
    limit = par[0]
    base = par[1]
    a = np.asarray(a)
    b = np.asarray(b)
    return (np.abs(np.log(a + 1) / np.log(base) - np.log(b + 1) / np.log(base)) < limit).sum() / a.shape[0]


def exact_acc(a: np.ndarray, b: np.ndarray, par: None):
    if len(a) == 0:
        return -0.1
    return (a == b).sum() / len(a)


def lt_percentage_err(original: np.ndarray, predicted: np.ndarray, limit: Tuple[float, float]):
    if not len(original):
        return 0
    original = np.asarray(original)
    predicted = np.asarray(predicted)
    limit_percentage = limit[0]
    limit_absolute = limit[1]
    percentage = np.abs(original - predicted) < original * limit_percentage
    small = np.abs(predicted - original) < limit_absolute
    return np.logical_or(percentage, small).sum() / len(original)
