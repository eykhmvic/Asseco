import argparse
import pickle
import sys

import numpy as np
import pandas as pd

from utils import filter_dataframe


def load_classifier(classifier_path):
    f = open(classifier_path, "rb")
    return pickle.load(f)


def main(classifier_path):
    classifier = load_classifier(classifier_path)
    columns = classifier["columns"]
    scaler = classifier["scaler"]
    classifier = classifier["classifier"]
    print(columns)
    while True:
        row = []
        for c in columns:
            inp = input()
            # print(inp)
            row += [float(inp)]
        row = np.asarray(row)
        row = row.reshape(1, -1)
        rr = scaler.transform(row)

        for fil, cls in classifier:
            df = pd.DataFrame(rr, columns=columns)
            ra = filter_dataframe(df, fil)[0]
            if len(ra):
                print(cls.predict(ra)[0])


if __name__ == '__main__':
    parser_ = argparse.ArgumentParser(
        description='Load classifier from file, and wait for input on stdin.')
    parser_.add_argument('--classifier_path', '-c', required=True, type=str,
                         help='path to file with classifier')
    params_ = parser_.parse_args(sys.argv[1:])

    main(params_.classifier_path)
