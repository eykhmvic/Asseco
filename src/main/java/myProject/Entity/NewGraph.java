package myProject.Entity;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@lombok.Data
public class NewGraph {
    @Getter
    @Setter
    private String dataSet;
    @Getter
    @Setter
    HashMap<String, OneAttribute> many_attributes = new HashMap<>();

    @Getter
    @Setter
    List<String> attributesWantToShow = new ArrayList<>();

    public void add(OneAttribute oneAttribute) {
        many_attributes.put(oneAttribute.getName(), oneAttribute);
        if (!attributesWantToShow.contains(oneAttribute.getName()))
            attributesWantToShow.add(oneAttribute.getName());
    }

    public void addColor(OneAttribute oneAttribute){
        many_attributes.put(oneAttribute.getName(), oneAttribute);
    }

    public void remove(String name) {
        many_attributes.remove(name);
    }

    public NewGraph createNewGraph(String[] attributes, NewGraph oldGraph) {
        NewGraph newGraph = new NewGraph();
        for (String s : attributes) {
            newGraph.add(oldGraph.getMany_attributes().get(s));
        }
        newGraph.add(oldGraph.getMany_attributes().get("color"));
        newGraph.add(oldGraph.getMany_attributes().get("penalty"));
        return newGraph;
    }
}
