package myProject.Entity;

import lombok.Data;

import java.util.Set;

@Data
public class ClassifierModel {
    String[] columns = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow", "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max", "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost", "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"};
}
