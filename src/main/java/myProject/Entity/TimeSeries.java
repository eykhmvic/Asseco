package myProject.Entity;

import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
public class TimeSeries {
    String dataset;
    List<String> dates;
    HashMap<String, List<String>> hashMap;
}
