package myProject.Entity;

import java.util.ArrayList;
import java.util.List;

@lombok.Data
    public class OneAttribute {
        public String name;
        public List<String> data;
        public String description_en;
        public String description_cz;
        public String units;
    }