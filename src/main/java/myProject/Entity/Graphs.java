package myProject.Entity;

import myProject.Tools.GraphReader;
import myProject.Tools.TimeReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Graphs {

    @Autowired
    GraphReader graphReader = new GraphReader();
    @Autowired
    TimeReader timeReader = new TimeReader();


    private NewGraph pribory;
    private NewGraph hudba;
    private NewGraph letadla;
    private TimeSeries pribory_time;
    private TimeSeries hudba_time;
    private TimeSeries letadla_time;

    public void initAll(boolean cache) {
        pribory = graphReader.start(cache, "pribory");
        System.out.println(pribory.getDataSet());
        hudba = graphReader.start(cache, "hudba");
        System.out.println(hudba.getDataSet());
        letadla = graphReader.start(cache, "letadla");
        System.out.println(letadla.getDataSet());



        pribory_time = timeReader.getTimeSeries(cache, "pribory" );
        hudba_time = timeReader.getTimeSeries(cache, "hudba" );
        letadla_time = timeReader.getTimeSeries(cache, "letadla");
    }

    public NewGraph getGraph(String name) {
        System.out.println("try to get graph: " + name);
        switch (name) {
            case "hudba":
                return hudba;
            case "letadla":
                return letadla;
            case "pribory":
                return pribory;
            default:
                return null;
        }
    }

    public TimeSeries getTimeSeries(String name) {
        System.out.println("try to get timeSeries: " + name);
        switch (name) {
            case "hudba":
                return hudba_time;
            case "letadla":
                return letadla_time;
            case "pribory":
                return pribory_time;
            default:
                return null;
        }

    }

    @PostConstruct
    public void init() {
        initAll(false);
    }

}
