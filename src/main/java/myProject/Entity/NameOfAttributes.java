package myProject.Entity;

import lombok.Data;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@Data
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class NameOfAttributes {
    String dataset;

    HashMap<String, NameDescription> many_attributes = new HashMap<>();

    @PostConstruct
    public void inPost(){
        System.out.println("Name Of Attrubutes was crearted!" );
    }

    @Data
    public class NameDescription {
        private String name;
        private String getDescription_cz;
        private String description_en;

        NameDescription(String name, String description_cz, String description_en) {
            this.name = name;
            this.getDescription_cz = description_cz;
            this.description_en = description_en;
        }
    }


    public void add(String name, String description_cz, String description_en) {
        many_attributes.put(name, new NameDescription(name, description_cz, description_en));
    }


}
