package myProject.Tools;


import myProject.Entity.TimeSeries;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TimeReader {

    private static TimeSeries readFromFile(String filename) throws IOException {
        System.out.println("read from csv file " + filename);
        TimeSeries timeSeries = new TimeSeries();
        List<String> dates;
        HashMap<String, List<String>> hashMap;
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/" + filename + ".csv";
        List<List<String>> lines = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(p));
        String line;
        int maxI = 200;
        int i = 0;
        while ((line = bufferedReader.readLine()) != null) {
            if (i++ > maxI) break;
//            System.out.println(line.length());
            List<String> splits = Arrays.asList(line.split(","));
//            String[] splits = line.split(",");
            lines.add(splits);
//            System.out.println(splits.length);
        }
        dates = lines.get(0)
                .subList(2, lines.get(0).size())
                .stream()
                .map(o -> convert(Double.valueOf(o).longValue()))
                .collect(Collectors.toList());
        hashMap = new HashMap<>();
        lines.subList(1, lines.size()).stream().forEach(o -> hashMap.put(o.get(0), o.subList(2, o.size())));
        timeSeries.setDates(dates);
        timeSeries.setHashMap(hashMap);
        return timeSeries;
    }

    private static String convert(long l) {
        Date date = new java.util.Date(l * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public TimeSeries getTimeSeries(String filename) throws IOException {
        return getTimeSeries(false, filename);
    }

    @SneakyThrows
    public TimeSeries getTimeSeries(boolean readFromCache, String dataset) {
        TimeSeries timeSeries;
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/" + dataset + "_data_cont" + ".json";
        ObjectMapper objectMapper = new ObjectMapper();
        if (readFromCache && Files.exists(Paths.get(p))) {
            System.out.println("read from merged! " + dataset);
            return objectMapper.readValue(new File(p), TimeSeries.class);
        } else {
            System.out.println("read  and write to file " + dataset);
            timeSeries = readFromFile(dataset + "_data_cont");
            timeSeries.setDataset(dataset);
            if (timeSeries.getDataset() == null) {
                System.exit(1);
            }
            objectMapper.writeValue(new File(p), timeSeries);
            return timeSeries;
        }
    }

    public static void main(String[] args) throws IOException, ParseException {

    }
}
