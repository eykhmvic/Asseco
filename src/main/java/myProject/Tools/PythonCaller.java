package myProject.Tools;

import lombok.SneakyThrows;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class PythonCaller {
    BufferedReader stdInput;
    BufferedReader stdError;
    Process p;
    String[] columns;
    int size;

    @SneakyThrows
    public void init() {
        String[] cmd = {"venv/Scripts/python.exe",
                "pythonModule/pythonDir/mypython.py"};
        p = Runtime.getRuntime().exec(cmd);
        columns = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow", "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max", "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost", "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"};
        size = columns.length;
//        HashSet<String> list = new HashSet<>(Arrays.asList(columns));
//        GraphReader graphReader = new GraphReader();
        //HashMap<String, List<String>> hashMap = graphReader.readingFile("letadla_manufacture_command_ex", list);

        stdInput = new BufferedReader(new
                InputStreamReader(p.getInputStream()));

        stdError = new BufferedReader(new
                InputStreamReader(p.getErrorStream()));
        stdInput.readLine();

    }

    public static void main(String[] args) throws IOException {
//
//
//        String outp;
//        double zero_count = 0;
//        double one_count = 0;
//        double two_count = 0;
//        System.out.println("size: " + hashMap.get("command_price").size());
//        //read columns names
//        stdInput.readLine();
//        for (int i = 0; i < hashMap.get("command_price").size(); i++) {
//            for (String s : strings) {
////                System.out.println(hashMap.get(s).get(i));
////                System.out.println("write: " + hashMap.get(s).get(i));
//                p.getOutputStream().write((hashMap.get(s).get(i) + "\n").getBytes());
////                p.getOutputStream().write((s + "\n").getBytes());
//            }
////            System.out.println("flush");
//            p.getOutputStream().flush();
//            for (String s : strings) {
//                String readLine = stdInput.readLine();
////                System.out.println("readLine " + readLine);
////                System.out.println(s + ": " + );
//            }
//            outp = stdInput.readLine();
////            System.out.println("read final: " + outp);
//            switch (outp) {
//                case "0":
//                    zero_count++;
////                    System.out.println("zero: " + zero_count);
//                    break;
//                case "1":
//                    one_count++;
////                    System.out.println("one: " + one_count);
//                    break;
//                case "2":
//                    two_count++;
//                    System.out.println("two: " + two_count);
//                    break;
//                default:
//                    System.out.println(i + " ... |" + outp);
//                    for (String s : strings) {
//                        System.out.println(hashMap.get(s).get(i));
//                    }
//            }
//
//        }
//        System.out.println("zero: " + zero_count);
//        System.out.println("one: " + one_count);
//        System.out.println("two: " + two_count);

    }

    public String evaluate(List<String> values) {
        try {
            if (values.size() != size) {
                System.out.println("Wrong size. Expected: " + size + " got: " + values.size());
                return null;
            } else {
                System.out.println("Size is ok!");
            }
            for (String s : values) {
//                System.out.println("writing: " + s);
                p.getOutputStream().write((s + "\n").getBytes());
//                stdInput.readLine();
            }
            p.getOutputStream().flush();
            return stdInput.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println("returning null ((");
        return null;
    }

}
