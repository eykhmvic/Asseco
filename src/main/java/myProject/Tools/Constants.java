package myProject.Tools;

import java.util.LinkedList;

public class Constants {
    public static final String MY_ATTRIBUTES_FILE_NAME = "desctiption_asseco_min";
    public static final String DEFAULT_DATASET = "pribory";
    public static final String[] COLUMNS_FOR_CLASSIFIER = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow", "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max", "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost", "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"};

}

