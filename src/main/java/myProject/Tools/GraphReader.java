package myProject.Tools;

import com.google.common.collect.Sets;
import myProject.Entity.NameOfAttributes;
import myProject.Entity.NewGraph;
import myProject.Entity.OneAttribute;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class GraphReader {

    @Autowired
    NameOfAttributes nameOfAttributes;

    //    1.Read my file. Get HashMap and NameOfAttributes
    @SneakyThrows
    private HashMap<String, List<String>> read_my_file(String filename) {
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/" + filename + ".csv";
        String line;
        String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
        HashMap<String, List<String>> map = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader(p));
        br.readLine();

        while ((line = br.readLine()) != null) {
            String[] splits = line.split(cvsSplitBy, -1);
            map.put(splits[0], new ArrayList<>());
            for (int i = 1; i < splits.length; i++) {
                map.get(splits[0]).add(splits[i]);
            }
        }
        for (String s : map.keySet()) {
            nameOfAttributes.add(s, map.get(s).get(1), map.get(s).get(2));
        }
        br.close();
        System.out.println("Was reading myFile!");
//        my_map = map;
        //setNameOfAttributes();
        return map;
    }


    //read to hash map  only nessesary attributes
    public HashMap<String, List<String>> readingFile(String filename, Set<String> nameOfMyAttributes) {
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/" + filename + ".csv";
        String line;
        String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
        HashMap<String, List<String>> hashMap = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(p))) {

            HashMap<Integer, String> index_name = new HashMap<>();

            line = br.readLine();
            String[] splits = line.split(cvsSplitBy, -1);
            for (int i = 0; i < splits.length; i++) {

                if (nameOfMyAttributes.contains(splits[i])) {
                    index_name.put(i, splits[i]);
                    hashMap.put(splits[i], new ArrayList());
                }

            }
            int maxI = 200;
            int curi = 0;
            while ((line = br.readLine()) != null) {
                if (curi++ > maxI) break;
                splits = line.split(cvsSplitBy, -1);
                for (int i = 0; i < splits.length; i++) {
                    if (index_name.containsKey(i)) {
                        String name = index_name.get(i);
                        hashMap.get(name).add(splits[i]);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("File readed!");
        return hashMap;
    }


    private NewGraph createGraph(HashMap<String, List<String>> my_map, HashMap<String, List<String>> hashMap) {
        NewGraph newGraph = new NewGraph();
        for (String s : hashMap.keySet()) {
//            if (s.equals("cost_penalty_60")) continue;
            OneAttribute att = new OneAttribute();
            att.name = s;
            att.units = my_map.get(s).get(0);
            att.description_cz = my_map.get(s).get(1);
            att.description_en = my_map.get(s).get(2);
            att.data = hashMap.get(s);
            newGraph.add(att);
        }
//        OneAttribute colorAttrubute = new OneAttribute();
//        colorAttrubute.name = "color";
//        colorAttrubute.units = "";
//        colorAttrubute.description_en = "color";
//        colorAttrubute.description_cz = "barva";
//        colorAttrubute.data = colorColumn(newGraph.getMany_attributes().get("penalty").data);
//        newGraph.add(colorAttrubute);
//
//        OneAttribute scaledColor = new OneAttribute();
//        scaledColor.name = "scaled_color";
//        scaledColor.units = "";
//        scaledColor.description_en = "color";
//        scaledColor.description_cz = "barva";
//        scaledColor.data = getColorScale();

//        newGraph.add(scaledColor);

        OneAttribute color = new OneAttribute();
        color.name = "color";
        color.units = "";
        color.description_cz = "GYR";
        color.description_en = "GYR";
        color.data = getColor(hashMap.get("cost_penalty_60"));
        newGraph.add(color);


        return newGraph;
    }

    public List<String> getColor(List<String> cost_penalty_60) {
        Stream<String> stream = cost_penalty_60.stream();
        List<String> colorList = new ArrayList<>();
        long size = cost_penalty_60.size();
        List<Long> list = stream.map(s -> Double.valueOf(s).longValue()).sorted().collect(Collectors.toList());
        int indexOneThird = (int) size / 3;
        int indexTwoThird = 2 * (int) size / 3;
        long valueOneThird = list.get(indexOneThird);
        long valueTwoThird = list.get(indexTwoThird);

        for (String s : cost_penalty_60) {
            long l = Double.valueOf(s).longValue();
            if (l < valueOneThird) {
                colorList.add("#00FF00");
            } else if (l < valueTwoThird) {
                colorList.add("#FFFF00");
            } else {
                colorList.add("#FF0000");
            }
        }
        return colorList;
    }

    public NewGraph start(String dataset) {
        return start(true, dataset);
    }


    @SneakyThrows
    public NewGraph start(boolean searchForCache, String dataset) {
        String path = "src/main/webapp/WEB-INF/views/" + dataset + "_merged.json";
        if (searchForCache && new File(path).exists()) {
            System.out.println("return from merged " + dataset);
            return readFromMerged(path);
        } else {
            System.out.println("Read and create again");
            HashMap<String, List<String>> my_map = read_my_file("desctiption_asseco_min");
            HashMap<String, List<String>> hashmap = readingFile(dataset + "_manufacture_command_ex", nameOfAttributes.getMany_attributes().keySet());
            NewGraph newGraph = createGraph(my_map, hashmap);
            newGraph.setDataSet(dataset);

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File("src/main/webapp/WEB-INF/views/" + dataset + "_merged.json"), newGraph);
            System.out.println("File writed! " + dataset);

            return newGraph;
        }
    }

    @SneakyThrows
    private static NewGraph readFromMerged(String filename) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(new File(filename), NewGraph.class);
    }


    public ArrayList<String> colorColumn(List<String> penalty) {
        ArrayList<String> colors = new ArrayList<>();
        for (String o : penalty) {
            double val = Double.valueOf(o);
            if (val == 0) {
                colors.add("#00FF00");
            } else if (val == 60) {
                colors.add("#FF0000");
            } else {
                colors.add("#FFFF00");
            }
        }
        return colors;
    }

    public ArrayList<String> getColorScale() {
        int green = 255;
        int red = 0;
        int stepSize = 255 / 29;
        ArrayList<String> green_red_colors = new ArrayList<>();

        while (red < 255) {
            red += stepSize;
            if (red > 255) red = 255;
            green_red_colors.add("#" + Integer.toHexString(0x100 | red).substring(1) + "" + Integer.toHexString(0x100 | green).substring(1) + "00");
//            System.out.println("rgb("+red+","+green+",0)");
        }
        while (green > 0) {
            green -= stepSize;
            if (green < 0) green = 0;
            green_red_colors.add("#" + Integer.toHexString(0x100 | red).substring(1) + "" + Integer.toHexString(0x100 | green).substring(1) + "00");
//            System.out.println("rgb("+red+","+green+",0)");
        }
        return green_red_colors;
    }

    public static void readAndWriteForClassifier() throws IOException {
        String[] strings = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow", "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max", "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost", "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"};
        HashSet<String> list = new HashSet<>(Arrays.asList(strings));
        GraphReader graphReader = new GraphReader();
        HashMap<String, List<String>> hashMap = graphReader.readingFile("pribory_manufacture_command_ex", list);
        for (int i = 0; i < 10; i++) {
            for (String s : strings) {
                System.out.println(hashMap.get(s).get(i));
            }
            System.out.println("\n\n\n");
        }
    }

    public static void main(String[] args) throws IOException {
//        GraphReader graphReader = new GraphReader();
//        graphReader.nameOfAttributes = new NameOfAttributes();
//        NewGraph newGraph = graphReader.start(false, "pribory");
//        System.out.println(newGraph.getMany_attributes().keySet());
        readAndWriteForClassifier();
    }


}
