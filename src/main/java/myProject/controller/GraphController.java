package myProject.controller;

import lombok.Data;
import myProject.Entity.*;
import myProject.Tools.Constants;
import myProject.Tools.GraphReader;
import myProject.Tools.PythonCaller;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Controller
@SessionScope
public class GraphController {

    private NewGraph currentGraph;
    TimeSeries currentTimeSeries;

    @Autowired
    NameOfAttributes nameOfAttributes;

    @Autowired
    Graphs graphs;


    @PostConstruct
    public void init() throws IOException {
        System.out.println("In postConstruct");
    }

    @RequestMapping("/GYR")
    public ModelAndView changeColor(@RequestParam(value = "column") String column, HttpSession session) {
        String dataset = (String) session.getAttribute("dataset");
        NewGraph graph = graphs.getGraph(dataset);
        graph.remove("color");
        OneAttribute color = new OneAttribute();
        color.name = "color";
        color.units = "";
        color.description_cz = "GYR";
        color.description_en = "GYR";
        color.data = getColor(graph.getMany_attributes().get(column).data);
        graph.addColor(color);
        currentGraph = graph;
        return new ModelAndView("newGraph.jsp", "graph", new JSONObject(currentGraph));
    }


    public List<String> getColor(List<String> cost_penalty_60) {
        Stream<String> stream = cost_penalty_60.stream();
        List<String> colorList = new ArrayList<>();
        long size = cost_penalty_60.size();
        List<Long> list = stream.map(s -> Double.valueOf(s).longValue()).sorted().collect(Collectors.toList());
        int indexOneThird = (int) size / 3;
        int indexTwoThird = 2 * (int) size / 3;
        long valueOneThird = list.get(indexOneThird);
        long valueTwoThird = list.get(indexTwoThird);

        for (String s : cost_penalty_60) {
            long l = Double.valueOf(s).longValue();
            if (l < valueOneThird) {
                colorList.add("#00FF00");
            } else if (l < valueTwoThird) {
                colorList.add("#FFFF00");
            } else {
                colorList.add("#FF0000");
            }
        }
        return colorList;
    }


    @RequestMapping(value = {"/GraphSettings"})
    public ModelAndView graphSettings(HttpSession session) {
        String dataset = (String) session.getAttribute("dataset");
        return new ModelAndView("GraphSettings.jsp", "graph", new JSONObject(nameOfAttributes));
    }


    @RequestMapping(value = "/graph", method = RequestMethod.POST)
    public ModelAndView hello(@RequestParam(value = "my_checkboxes[]", required = false) String[] strings) {
        currentGraph.setAttributesWantToShow(Arrays.asList(strings));
        return new ModelAndView("newGraph.jsp", "graph", new JSONObject(currentGraph));

    }

    @RequestMapping(value = {"/", "/main"})
    public ModelAndView main(HttpSession session, @RequestParam(value = "dataset", required = false) String dat, @RequestParam(value = "type", required = false) String type) {
        String dataset;
        if (dat != null && !dat.equals("")) {
            dataset = dat;
            session.setAttribute("dataset", dat);
        } else if (session.getAttribute("dataset") != null) {
            dataset = (String) session.getAttribute("dataset");
        } else {
            session.setAttribute("dataset", Constants.DEFAULT_DATASET);
            dataset = Constants.DEFAULT_DATASET;
        }
        currentGraph = graphs.getGraph(dataset);
        currentTimeSeries = graphs.getTimeSeries(dataset);

        if (type != null) {
            switch (type) {
                case "time":
                    currentTimeSeries = graphs.getTimeSeries(dataset);
                    session.setAttribute("type", "time");
                    return new ModelAndView("TimeSeries.jsp", "timeSeries", new JSONObject(currentTimeSeries));
                case "statistic":
                default:
                    session.setAttribute("type", "statistic");
                    currentGraph = graphs.getGraph(dataset);
                    return new ModelAndView("newGraph.jsp", "graph", new JSONObject(currentGraph));
            }
        } else if (session.getAttribute("type") != null) {
            if (session.getAttribute("type").equals("time")) {
                return new ModelAndView("TimeSeries.jsp", "timeSeries", new JSONObject(currentTimeSeries));

            } else {
                return new ModelAndView("newGraph.jsp", "graph", new JSONObject(currentGraph));

            }
        } else {
            session.setAttribute("type", "statistic");
            currentGraph = graphs.getGraph(dataset);
            return new ModelAndView("newGraph.jsp", "graph", new JSONObject(currentGraph));

        }
//        if (!session.getAttribute("dataset").equals((String) request.getAttribute("dataset"))) {
//            currentGraph = graphReader.start(false, "pribory");
//        }
//        request.setAttribute("dataset", "pribory");
    }

    @RequestMapping("/newGraph")
    public ModelAndView i() {
        currentGraph = graphs.getGraph("pribory");
        return new ModelAndView("newGraph.jsp", "graph", currentGraph);
    }

    @RequestMapping(value = "/classifier")
    public ModelAndView getClassifer() {
        return new ModelAndView("classifier.jsp", "modelName", new JSONArray(Constants.COLUMNS_FOR_CLASSIFIER));
    }

    @RequestMapping(value = "/simple", method = RequestMethod.POST)
    @ResponseBody
    public String simple(@RequestParam Map<String, String> allRequestParams, HttpServletResponse response, HttpServletRequest request) {
        response.setContentType("text/plain; charset=utf-8");
        System.out.println("Get from simple!!! " + allRequestParams);
        PythonCaller pythonCaller = new PythonCaller();
        pythonCaller.init();
        LinkedList<String> columns = new LinkedList<>();
        for (String s : Constants.COLUMNS_FOR_CLASSIFIER) {
            columns.add(allRequestParams.get(s));
        }
        String s = pythonCaller.evaluate(columns);
        System.out.println("Inside simple!! " + s);
//        return s;
//        columns.add(s);
        LinkedList<String> strings = new LinkedList<>(Arrays.asList(Constants.COLUMNS_FOR_CLASSIFIER));
        strings.add(s);
//        return new ModelAndView("classifier.jsp", "modelName", new JSONArray(strings));
        return s;
    }


    @RequestMapping(value = "/TimeSeries")
    @ResponseBody
    public ModelAndView timeSeries(HttpServletResponse response, HttpSession session) {
        response.setContentType("text/plain; charset=utf-8");

        if (session.getAttribute("dataset") != null) {
            String dataset = (String) session.getAttribute("dataset");
            currentTimeSeries = graphs.getTimeSeries(dataset);
        } else {
            currentTimeSeries = graphs.getTimeSeries(Constants.DEFAULT_DATASET);
        }
        return new ModelAndView("TimeSeries.jsp", "timeSeries", new JSONObject(currentTimeSeries));
    }


}
