package myProject.Weka;

import com.google.common.collect.Sets;
import myProject.Tools.GraphReader;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyWekaDemo {
    public void writeToWeka() throws IOException {
        String[] nessesaryNames = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow",
                "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max",
                "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost",
                "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces", "cost_penalty_60"};

        GraphReader graphReader = new GraphReader();
        String dataset = "pribory";
        HashMap<String, List<String>> hashMap = graphReader.readingFile(dataset + "_manufacture_command_ex", Sets.newHashSet(nessesaryNames));


        Stream<String> stream = hashMap.get("cost_penalty_60").stream();
        long size = hashMap.get("cost_penalty_60").size();
        List<Long> list = stream.map(s -> Double.valueOf(s).longValue()).sorted().collect(Collectors.toList());
        int indexOneThird = (int) size / 3;
        int indexTwoThird = 2 * (int) size / 3;
        long valueOneThird = list.get(indexOneThird);
        long valueTwoThird = list.get(indexTwoThird);

        System.out.println(indexOneThird + "\t" + indexTwoThird + "\t" + size);
        System.out.println(valueOneThird + "\t" + valueTwoThird + "\t" + list.get(list.size() - 1));
        hashMap.put("GYR", new ArrayList<String>());
        for (String s : hashMap.get("cost_penalty_60")) {
            long l = Double.valueOf(s).longValue();
            if (l < valueOneThird) {
                hashMap.get("GYR").add("G");
            } else if (l < valueTwoThird) {
                hashMap.get("GYR").add("Y");
            } else {
                hashMap.get("GYR").add("R");
            }
        }
        hashMap.remove("cost_penalty_60");

        //writing to file
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/";
//        Paths.get()
        BufferedWriter bw = new BufferedWriter(new FileWriter(p + "priboryWeka.arff"));
        bw.write("@RELATION myRelation\n\n");
//        bw.write(hashMap);
//        @ATTRIBUTE class 	{Iris-setosa,Iris-versicolor,Iris-virginica}
        for (String s : hashMap.keySet()) {
            if (s.equals("GYR")) {
                bw.write("@ATTRIBUTE class {G,Y,R}\n");
            } else
                bw.write("@ATTRIBUTE " + s + "\tREAL" + "\n");
        }
        bw.write("\n@DATA\n");
        for (int i = 0; i < hashMap.get("parent_count").size(); i++) {
            for (String s : hashMap.keySet()) {
                bw.write(hashMap.get(s).get(i) + ", ");
            }
            bw.write("\n");
        }
        bw.close();
    }

    public static void main(String[] args) throws Exception {
        String p = Paths.get("").toAbsolutePath().toString() + "/src/main/webapp/WEB-INF/views/data/" + "priboryWeka.arff";

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(p);
        Instances data = source.getDataSet();
        data.setClassIndex(0);

        String[] options = new String[1];
        options[0] = "-U";            // unpruned tree
        J48 tree = new J48();         // new instance of tree
        tree.setOptions(options);     // set the options
        tree.buildClassifier(data);   // build classifier

        Evaluation eval = new Evaluation(data);
        eval.crossValidateModel(tree, data, 10, new Random(1));

//        Classifier cls = new J48();
//        cls.buildClassifier(data);
//         evaluate classifier and print some statistics
//        Evaluation eval = new Evaluation(train);
//        eval.evaluateModel(cls, test);
        System.out.println(eval.toSummaryString("\nResults\n======\n", false));

    }

}
