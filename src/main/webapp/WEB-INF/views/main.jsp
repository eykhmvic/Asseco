<%--
     Created by IntelliJ IDEA.
     User: veykhmann
     Date: 13.03.2018
     Time: 10:02
     To change this template use File | Settings | File Templates.
     --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Asseco</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Home</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Type
                            <b class="caret"> </b>
                            <ul class="dropdown-menu">
                                <li><a href="/newGraph"> Statistic</a> </li>
                                <li><a href="/TimeSeries"> Time series</a> </li>
                            </ul>
                            </li>
                </ul>
            </div>
        </nav>
        <h1 class="text-center"> Welcome to Asseco Graphs</h1>
        <center>
            <form action="fromMain" method="POST">
                <h2>Choose dataset</h2>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-success active"> Pribory
                        <input type="radio" name="dataset" id="pribory"  value="pribory" autocomplete="off" checked>
                    </label>
                    <label class="btn btn-success"> Hudba
                        <input type="radio" name="dataset" id="hudba" value="hudba" autocomplete="off">
                    </label>
                    <label class="btn btn-success"> Letadla
                        <input type="radio" name="dataset" id="letadla" value="letadla" autocomplete="off">
                    </label>
                </div>
                <h2>Choose graph type</h2>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-success active"> Graph
                        <input type="radio" name="graphOrTime" id="hudba" value="graph" autocomplete="off" checked>
                    </label>
                    <label class="btn btn-success"> Time
                        <input type="radio" name="graphOrTime" id="letadla" value="time" autocomplete="off">
                    </label>
                </div>
                <br>
                <br>
                <input type="submit" class="btn btn-primary">
            </form>
        </center>
    </body>
</html>