<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<%--<head>--%>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <%--<title>Spring 4 MVC - Hello World Example | BORAJI.COM</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<h2>${message}</h2>--%>
<%--<h4>Server date time is : ${date} </h4>--%>
<%--</body>--%>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<canvas id="myChart"></canvas>

<script src="https://cdn.jsdelivr.net/npm/flexio-sdk-js@1.15.1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<script>
    var ctx = document.getElementById("myChart");

    var firstData = {
        label: '${label1}',
        column2Data: ${data1},
        lineTension: 0,
        borderColor: 'orange',
        backgroundColor: 'transparent',
        pointBorderColor: 'orange',
        pointBackgroundColor: 'orange',
        pointRadius: 5,
        pointHoverRadius: 10,
        pointHitRadius: 30,
        pointBorderWidth: 2
    };
    var secondData = {label: '${label2}',
        column2Data: ${data2},
        lineTension: 0,
        borderColor: 'blue',
        backgroundColor: 'transparent',
        pointBorderColor: 'blue',
        pointBackgroundColor: 'blue',
        pointRadius: 5,
        pointHoverRadius: 10,
        pointHitRadius: 30,
        pointBorderWidth: 2
    };
    var thirdData = {label: '${label3}',
        column2Data: ${data3},
        lineTension: 0,
        borderColor: 'green',
        backgroundColor: 'transparent',
        pointBorderColor: 'green',
        pointBackgroundColor: 'green',
        pointRadius: 5,
        pointHoverRadius: 10,
        pointHitRadius: 30,
        pointBorderWidth: 2
    };

    var all_data = {
        column1Data: ${column1Data},
        datasets: [firstData, secondData, thirdData]
    };


    var lineChart = new Chart(ctx, {
        type: 'line',
        column2Data: all_data,
        options: {
            showLines: false
        }

    });


</script>



</body>




</html>
