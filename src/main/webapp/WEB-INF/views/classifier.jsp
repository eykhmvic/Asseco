
<!--<%@ page contentType="text/html;charset=UTF-8" language="java" %>-->
<html>
    <head>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/graphs_settings.css">
        <style>
            .red_circle
            {
                width: 100px;
                height: 100px;
                border-radius: 50%;
                border: 1px solid;
                background-color: #ff0000;
                opacity: 0.3;
            }
            .yellow_circle
            {
                width: 100px;
                height: 100px;
                border-radius: 50%;
                border: 1px solid;
                background-color: #ffff00;
                opacity: 0.3;
            }
            .green_circle
            {
                width: 100px;
                height: 100px;
                border-radius: 50%;
                border: 1px solid;
                background-color: #00ff00;
                opacity: 0.3;
            }
            .gray_circle
            {
                width: 100px;
                height: 100px;
                border-radius: 50%;
                border: 1px solid;
                background-color: #555555;
                opacity: 0.3;
            }
        </style>
    </head>
    <body>

        <h1 class="text-center">Write classifer parameters</h1>
        <div class="container">
            <div class="row" >
                <form>
                    <div class=" form-group col-md-5" id="myForm">



                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="gray_circle col-md-3" id="myCircle"> </div>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-primary" id="btn-rand">RANDOM</button>
                        </div>
                        <div class="row">
                            <br>
                            <input type="submit" class="btn btn-primary" value="Request data" id="request_data">

                        </div>


                    </div>

                    <!--                <p id="demo"></p>-->
                </form >
            </div>
        </div>

        <script>

        </script>




        <script>
            function getRandomNumber(){
                var max = 1000.0, min = 0.0;
                return Math.random() * (max - min) + min; 
            }
        </script>
        <script>

            cols = ${modelName};
            for(s in cols){
                s = cols[s];
                if(s=="1" || s=="2" || s=="0"){
                    console.log("s = " + s);
                    continue;
                }

                var lab = document.createElement("label");
                lab.setAttribute("for", s)
                lab.innerHTML = s;

                var inp = document.createElement("input");
                inp.setAttribute("type", "number");
                inp.setAttribute("step","any");
                inp.setAttribute("name", s);
                inp.required = true;
                inp.setAttribute("class","form-control");
                inp.setAttribute("placeholder", s);

                var br = document.createElement("br");

                document.getElementById("myForm").appendChild(lab);
                document.getElementById("myForm").appendChild(inp);
                document.getElementById("myForm").appendChild(br);
            }

            var ran = document.getElementById("btn-rand");
            ran.addEventListener("click", function(e){

                var nodes = document.querySelectorAll("#myForm input[type=number]");
                for (var i=0; i<nodes.length; i++)
                    nodes[i].value = getRandomNumber();


            });

        </script>
        <script>
            $("#request_data").click(function (event) {
                event.preventDefault();
                loadDoc();
            })

            function loadDoc() {

                var circle = document.getElementById("myCircle");
                circle.className="";
                circle.classList.add("gray_circle");
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        //                        document.getElementById("demo").innerHTML = this.responseText;
                        var circle = document.getElementById("myCircle");
                        circle.className="";
                        if(this.responseText=="0"){
                            circle.classList.add("green_circle");
                        }else if(this.responseText == "1"){
                            circle.classList.add("yellow_circle");
                        }else if(this.responseText == "2"){
                            circle.classList.add("red_circle");
                        }
                    }
                };
                xhttp.open("POST", "/simple", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var sending_string = "";
                var nodes = document.querySelectorAll("#myForm input[type=number]");
                for (var i=0; i<nodes.length; i++){
                    sending_string += nodes[i].name + "="+nodes[i].value + "&";
                }
                //                console.log(sending_string);
                xhttp.send(sending_string);
            }


        </script>




    </body>
</html>
