<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Graph</title>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>



<canvas id="myChart"></canvas>
<div id="myDiv" style="width:600px;height:250px;"></div>
<script>
       for (var i = 0; i < 50; i ++) {
        y0[i] = Math.random();
        y1[i] = Math.random() + 1;
    }

    var trace1 = {
        y: y0,
        type: 'box'
    };

    var trace2 = {
        y: y1,
        type: 'box'
    };



    var data = [trace1, trace2];

    Plotly.newPlot('myDiv', data);


</script>
<script src="https://cdn.jsdelivr.net/npm/flexio-sdk-js@1.15.1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<script>
    var ctx = document.getElementById("myChart");
    var firstData = {
        label: '${graphInst.columnName1}',
        data: ${graphInst.column1Data},
        lineTension: 0,
        borderColor: 'orange',
        backgroundColor: 'transparent',
        pointBorderColor: 'orange',
        pointBackgroundColor: 'orange',
        pointRadius: 2,
        pointHoverRadius: 10,
        pointHitRadius: 5,
        pointBorderWidth: 2
    };


    <%--var lineChart = new Chart(ctx, {--%>
    <%--type: 'bar',--%>
    <%--data: {--%>
    <%--labels: ${graphInst.column2Data},--%>
    <%--datasets: [firstData]--%>
    <%--},--%>
    <%--options: {--%>
    <%--showLines: false--%>
    <%--}--%>

    <%--});--%>


    var chartOptions = {
        scales: {
            yAxes: [{
                barPercentage: 0.5,
                gridLines: {
                    display: false
                }

            }],
            xAxes: [{
                gridLines: {
                    zeroLineColor: "black",
                    zeroLineWidth: 2
                },

                scaleLabel: {
                    display: true,
                    labelString: "Density in kg/m3"
                }
            }]
        },
        elements: {
            rectangle: {
                borderSkipped: 'left',
            }
        }
    };

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ${graphInst.column2Data},
            datasets: [
                {
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
                    data: ${graphInst.column1Data}
                }
            ]
        },
        options: chartOptions
    });


</script>

<%--<script>
    var x = new Chart(document.getElementById("myChart"), {
        type: 'scatter',
        data: {
            datasets: [{
                label: "Test",
                data: [
                    <c:forEach var="Array" items="${A}" varStatus="status">
                    {
                        x:<c:out value="${A[status.index]}"/>,
                        y:<c:out value="${B[status.index]}"/>
                    },
                    </c:forEach>
                ],
            }]
        },
        options: {
            responsive: true,
            showLines: false,
        }
    });
</script>--%>


</body>


</html>
