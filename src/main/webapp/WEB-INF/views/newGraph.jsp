<!--<%@ page contentType="text/html;charset=UTF-8" language="java" %>-->
<html>
    <head>
        <title>newGraph</title>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
        <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
        <link rel="stylesheet" href="resources/css/main_style.css">

    </head>
    <body>

        <script>
            var myHashMap = ${graph}["many_attributes"];
            var attributesWantToShow = ${graph}["attributesWantToShow"];
        </script>

        <!--
<nav class="navbar navbar-light bg-light">
<a class="navbar-brand" href="#">
<img src="/resources/img/asseco-logo.jpg" height="60" class="d-inline-block align-top" alt=""> Asseco
</a>
</nav>
-->


        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Asseco</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="#">Description <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">UML Model</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GYR <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/GYR?column=cost_penalty_60">penalty_cost_60</a></li>
                                <li><a href="/GYR?column=actual_cost">actual_cost</a></li>
                                <li><a href="/GYR?column=cost_sin_2_6_60">cost_sin_2_6_60</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/GraphSettings" >Settings</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>









        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified">

                <li class="nav-item active">
                    <a class="nav-link"  href="/main?type=statistic">Statistic</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="/main?type=time">Time</a>
                </li>
            </ul>
        </div>


        <div class="container-fluid">

            <div class="row" style="height: 100%">
                <div class="col-sm-1 col-md-1 col-lg-1" style="width: 5%; height: 100%">
                    <ul class="nav flex-column ">
                        <li class="nav-item">
                            <a class="nav-link active" href="/main?dataset=pribory&type=statistic" style="height: 33.333%;">Pribory</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/main?dataset=hudba&type=statistic" style="height: 33.333%;">Hudba</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/main?dataset=letadla&type=statistic" style="height: 33.333%;">Letadla</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" id="x_y_columns">

                    <div class=" btn-group col-sm-auto col-md-auto col-lg-auto btn-group-vertical" data-toggle="buttons" id="x_buttons">
                        <label class="btn btn-info disabled" id="home">
                            <a>X</a>
                        </label>
                    </div>
                    <div class="btn-group col-sm-auto col-md-auto col-lg-auto btn-group-vertical" data-toggle="buttons" id="y_buttons">
                        <label class="btn btn-info disabled" id="home2">
                            <a>Y</a>
                        </label>
                    </div>


                </div>
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <center>
                        <h3 id="my_h1"></h3>
                    </center>
                    <div class="row">
                        <div class="col-sm-6" id="main">
                            <div class="checkbox">

                                <label>
                                    <input id="main_x_checkbox" type="checkbox" data-toggle="toggle" onchange="main_x_checkbox()"
                                           checked="checked">
                                    x log
                                </label>
                                <label>
                                    <input id="main_y_checkbox" type="checkbox" data-toggle="toggle" onchange="main_y_checkbox()"
                                           checked="checked">
                                    y log
                                </label>
                            </div>

                        </div>
                        <div class="col-sm-6" id="hist_right">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" id="hist_left">
                        </div>

                        <div class="col-sm-3" id="info">
                        </div>
                        <div class="col-sm-3" id="info2">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            document.getElementById("my_h1").innerHTML=${graph}["dataSet"];
        </script>
        <script src="resources/js/newGraph.js"></script>
    </body>
</html>
