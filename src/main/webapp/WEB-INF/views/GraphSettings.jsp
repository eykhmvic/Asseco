
<!--<%@ page contentType="text/html;charset=UTF-8" language="java" %>-->
<html>
    <head>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/graphs_settings.css">
    </head>
    <body>
        <script>
            function validateForm(){
                var j = 0;
                checkboxes = document.getElementsByName("my_checkboxes[]");
                for (i = 0; i < checkboxes.length; i++){
                    if(checkboxes[i].checked==true){
                        j = j+1;
                        if(j>=2){
                            return true;
                        }
                    }

                }
                document.getElementById("errorMessgae").innerHTML="at least two values"
                return false;
            }
        </script>
        <h1 class="text-center">Choose attribute you want to see</h1>
        <div class="container">
            <div class="row">
                <form action="graph", method="POST" onsubmit="return validateForm()">
                    <input type="hidden" name="dataset" id="dataset">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Choose attribute to show</div>
                            <!-- List group -->
                            <ul class="list-group" id="my_list_group">
                                <li class="list-group-item" >select all
                                    <div class="material-switch pull-right" >
                                        <input type="checkbox" id="select_all" checked="checked" onClick="toggle(this)">
                                        <label for="select_all" class="label-primary " ></label>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <input type="submit" class="btn btn-primary" id="mySubmit">
                        <p style="color: red" id="errorMessgae"></p>
                    </div>
                </form>
            </div>
        </div>


        <script >
            function toggle(source) {
                checkboxes = document.getElementsByName("my_checkboxes[]");
                for (i = 0; i < checkboxes.length; i++){
                    checkboxes[i].checked = source.checked;
                }
            }

        </script>
        <script>
            document.getElementById("dataset").value = ${graph}["dataset"];
            //console.log("Hello");
            //console.log(${graph});
            myHashMap = ${graph}["many_attributes"];
            for(s in myHashMap){
                if(myHashMap["name"]=="color" || myHashMap[s]["name"]=="penalty" || myHashMap[s]["name"]=="scaled_color" || myHashMap[s]["name"]=="product_name"|| myHashMap[s]["name"]=="cost_penalty_60"){
                    continue;
                }
                var inp = document.createElement("INPUT");
                inp.setAttribute("type", "checkbox");
                inp.setAttribute("name","my_checkboxes[]");
                inp.setAttribute("id", myHashMap[s]["name"]);
                inp.setAttribute("value", myHashMap[s]["name"]);
                inp.setAttribute("checked", "checked");


                var lab = document.createElement("LABEL");
                lab.setAttribute("for", myHashMap[s]["name"]);
                lab.setAttribute("class", "label-primary");

                var div1 = document.createElement("div");
                div1.setAttribute("class", "material-switch pull-right");

                var li = document.createElement("li");
                li.setAttribute("class", "list-group-item");
                li.setAttribute("title", myHashMap[s]["description_en"]);

                var text =  document.createTextNode(myHashMap[s]["name"]);

                div1.appendChild(inp);
                div1.appendChild(lab);
                li.appendChild(text);
                li.appendChild(div1);

                document.getElementById("my_list_group").appendChild(li);
            }

        </script>
    </body>
</html>
