
<!--<%@ page contentType="text/html;charset=UTF-8" language="java" %>-->

<html>
    <head>
        <title>Time series</title>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="resources/css/main_style.css">
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Asseco</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="#">Description <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">UML Model</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/GraphSettings" >Settings</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified">

                <li class="nav-item ">
                    <a class="nav-link"  href="/main?type=statistic">Statistic</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/main?type=time">Time</a>
                </li>
            </ul>
        </div>


        <div class="container-fluid">

            <div class="row">
                <div class="col-md-1" style="width: 5%; height: 100%">
                    <ul class="nav flex-column ">
                        <li class="nav-item">
                            <a class="nav-link active" href="/main?dataset=pribory&type=time" style="height: 33.333%;">Pribory</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/main?dataset=hudba&type=time" style="height: 33.333%;">Hudba</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/main?dataset=letadla&type=time" style="height: 33.333%;">Letadla</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-10"  style="width: 80% height: 100%">
                    <center>
                        <h3 id="myTitleID"></h3>
                    </center>

                    <div id="myDiv">

                    </div>
                    <button type="button" onclick="plotPreviousGraph()" class="btn btn-danger col-md-1 col-md-offset-5">Previous 10</button>
                    <button type="button" onclick="plotNextGraph()" class="btn btn-primary col-md-1">Next 10</button>
                </div>
            </div>
        </div>






        <script>
            document.getElementById("myTitleID").innerHTML = ${timeSeries}["dataset"];
            console.log(${timeSeries}["dataset"]);
            var dates = ${timeSeries}["dates"];
            var hashMap = ${timeSeries}["hashMap"];
            //console.log("hashmap: " + ${timeSeries}["hashMap"]["9993624376001"]);
            //console.log(dates);
            //console.log(hashMap[Object.keys(hashMap)[0]]);
            //interesting id   2006833300270
            //console.log(Object.keys(hashMap)[0]);
            console.log(Object.values(hashMap)[0]);
            console.log(dates);
            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }

            var layout = {
                title: ${timeSeries}["dataset"],
                xaxis: {
                    title: "time",
                        autorange: true,
                            //range: [new Date(Math.min.apply(null,dates)), new Date(Math.max.apply(null,dates))],
                            type: 'date',
                                rangeselector: {buttons: [
                                    {
                                        count: 6,
                                        label: '6m',
                                        step: 'month',
                                        stepmode: 'backward'
                                    },
                                    {
                                        count: 12,
                                        label: 'year',
                                        step: 'month',
                                        stepmode: 'backward'
                                    },
                                    {step: 'all'}
                                ]},
                                    rangeslider: true,
                },
                    yaxis: {
                        autorange: true,
                            type: 'linear'
                    }
                };


            function getData(from, count){
                function getTrace(n){
                    trace =
                        {
                        name: Object.keys(hashMap)[n],
                        //name: "First",
                        x: dates,
                        y: Object.values(hashMap)[n],
                        type: "scatter",
                        mode: "lines",
                        line: {color: getRandomColor()}
                    }
                    return trace;
                }
                var data = [];

                for(i = from; i < from + count; i++){
                    data.push(getTrace(i));
                }
                return data;
            }

            var globalCount = 10;
            var globalFrom = -10;
            
            function plotNextGraph(){
                if(globalFrom+10+globalCount>Object.keys(hashMap).length) return;
                globalFrom = globalFrom + 10;
                data = getData(globalFrom, globalCount);
                Plotly.newPlot('myDiv', data, layout);
            }
            function plotPreviousGraph(){
                console.log(globalFrom);
                if(globalFrom<=0) return;
                globalFrom = globalFrom - 10;
                data = getData(globalFrom, globalCount);
                Plotly.newPlot('myDiv', data, layout);
            }

            plotNextGraph();

        </script>


    </body>
</html>
