<%--
  Created by IntelliJ IDEA.
  User: angelina
  Date: 26.2.18
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MyGraph</title>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        .sidenav {
            height: 100%;
            width: 160px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            overflow-x: hidden;
            /*padding-top: 20px;*/
        }

        .upnav {
            margin-left: 160px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            overflow-x: hidden;
        }

        .main {
            margin-left: 160px; /* Same as the width of the sidenav */
            font-size: 28px; /* Increased text to enable scrolling */
            /*padding: 0px 10px;*/
        }

    </style>
</head>
<body>


<div class="sidenav">
    <div class="btn-group-vertical " style="width: 100%;" data-toggle="buttons">

        <label class="btn btn-info" id="home">
            <input type="radio" name="options"> HOME
        </label>
        <label class="btn btn-primary" id="1">
            <input type="radio" name="options"> Left1
        </label>
        <label class="btn btn-primary" id="q">
            <input type="radio" name="options"> Left2
        </label>
        <label class="btn btn-primary" id="w">
            <input type="radio" name="options"> Left2
        </label>
    </div>

</div>

<div class="main ">

    <div class="upnav">
        <div class="btn-group  btn-group-justified" data-toggle="buttons">
            <label class="btn btn-primary " id="amount_produced">
                <input type="radio" name="options" id="e"> amount_produced
            </label>
            <label class="btn btn-primary" id="amount_rejected">
                <input type="radio" name="options"> amount_rejected
            </label>
            <label class="btn btn-primary" id="t">
                <input type="radio" name="options"> Horizontal3
            </label>
        </div>
    </div>
    <div id="myDiv"></div>
    <div id="hist"></div>

</div>

<%--<button id="all_in_one_button">Change data</button>--%>
<script>
    var trace1 = {
        x: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        y: [8, 7, 6, 5, 4, 3, 2, 1, 0],
        type: 'scatter'
    };
    var trace2 = {
        x: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        y: [0, 1, 2, 3, 4, 5, 6, 7, 8],
        type: 'scatter'
    };
    var data = [trace1, trace2];
    var layout = {
        xaxis: {
            showgrid: true,
            zeroline: true,
            showline: true,
            mirror: 'ticks',
            gridcolor: '#bdbdbd',
            gridwidth: 2,
            zerolinecolor: '#FF0000',
            zerolinewidth: 4,
            linecolor: '#636363',
            linewidth: 6,
            range:[1,5]
        },
        yaxis: {
            showgrid: true,
            zeroline: true,
            showline: true,
            mirror: 'ticks',
            gridcolor: '#bdbdbd',
            gridwidth: 2,
            zerolinecolor: '#969696',
            zerolinewidth: 4,
            linecolor: '#636363',
            linewidth: 6
        }
    };
    Plotly.newPlot('myDiv', data, layout);
    var dates = {
        x: ${g.getColumn("cost_sin_2_6_60")},
        y: ${g.getColumn("command_price")},
        mode: 'markers',
        type: 'scatter',
        name: 'cost penalty',
        marker: {
            size: 3,
            color: ${g.getColumn("color")},
            opacity: 0.8
        }
    };
    var data = [dates];
    var layout = {
        title: 'cost penalty on command_price',
        xaxis: {
            title: 'cost penalty',
            type: 'log',
            autorange: true
        },
        yaxis: {
            title: 'command_price',
            type: 'log',
            autorange: true
        }
    };
    Plotly.newPlot('all_in_one', data, layout);
</script>

<script>
    document.getElementById("amount_produced").addEventListener('click', function (e) {

        console.log("amount_produced");
        // repolot main graph
        var data_update = {
            mode: 'markers',
            type: 'scatter',
            name: 'cost penalty',
            marker: {
                size: 3,
                color: ${g.getColumn("color")},
                opacity: 0.8
            }
        };
        var layout_update = {
            title: 'penalty on amount_produced', // updates the title
            xaxis: {
                title: 'cost penalty',
                type: 'log',
                autorange: true
            },
            yaxis: {
                title: 'amount_produced',
                type: 'log',
                autorange: true
            }
        };

        Plotly.deleteTraces('all_in_one', 0);
        Plotly.addTraces('all_in_one', [{x: ${g.getColumn("cost_sin_2_6_60")}, y: ${g.getColumn("amount_produced")}}]);
        Plotly.update('all_in_one', data_update, layout_update);

        //replot histogram

        Plotly.deleteTraces('hist', 0);
        Plotly.addTraces('hist', [{y:${g.getColumn("cost_sin_2_6_60")}}]);
        var new_layout = {
            title: 'penalty hist',
            xaxis: {
                type: 'linear',
                autorange: true
            },
            yaxis: {
                type: 'linear',
                autorange: true
            }
        };
        Plotly.relayout('hist', new_layout);
    })
</script>

<script>
    document.getElementById("amount_rejected").addEventListener('click', function (e) {
        var data_update = {
            mode: 'markers',
            type: 'scatter',
            name: 'cost penalty',
            marker: {
                size: 3,
                color: ${g.getColumn("color")},
                opacity: 0.8
            }
        };
        var layout_update = {
            title: 'amount_rejected on command_price', // updates the title
            xaxis: {
                title: 'amount_rejected',
                type: 'linear',
                autorange: true
            },
            yaxis: {
                title: 'command_price',
                type: 'log',
                autorange: true
            }
        };

//        Plotly.newPlot('all_in_one', data_update, layout_update);
        Plotly.deleteTraces('all_in_one', 0);
        Plotly.addTraces('all_in_one', [{x: ${g.getColumn("amount_rejected")}, y: ${g.getColumn("command_price")}}]);
        Plotly.update('all_in_one', data_update, layout_update);

    })
</script>


<%--
            HISTOGRAMS
--%>

<script>
    var x = ${g.getColumn("penalty")};
    var trace = {
        x: x,
        type: 'histogram',
        marker: {
            color: ${g.getColumn("color_scale")},
        }
    };
    var data = [trace];
    var layout = {
        title: 'penalty hist',
        xaxis: {
            type: 'linear',
            autorange: true
        },
        yaxis: {
            type: 'linear',
            autorange: true
        }
    };
    Plotly.newPlot('hist', data, layout);
</script>


<script>
    document.getElementById("amount_rejected").addEventListener('click', function (e) {
        var layout_update = {
            title: 'amount_rejected hist',
            xaxis: {
                title: 'amount rejected',
                type: 'linear',
                autorange: true
            },
            yaxis: {
                title: 'count',
                type: 'log',
                autorange: true
            }
        };

        var new_trace = {
            x: ${g.getColumn("amount_rejected")},
            type: 'histogram'
        };


        Plotly.deleteTraces('hist', 0);
        Plotly.addTraces('hist', [new_trace]);
        Plotly.relayout('hist', layout_update);
//        Plotly.update('hist',new_trace, layout_update);

    })
</script>


</body>
</html>
