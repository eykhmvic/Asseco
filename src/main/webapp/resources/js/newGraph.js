var main_x_type = "log";
var main_y_type = "log";
var right_hist_x_type = "linear";
var rigth_hist_y_type = "log";
var left_hist_x_type = "linear";
var left_hist_y_type = "log";

var current_x = "";
//    Object.keys(myHashMap)[0];
var current_y = "";
//    Object.keys(myHashMap)[1];
var my_height = 400;
var my_width = 900;

var taken = 0;
for (var i=0;i<attributesWantToShow.length; i++) {
    s = attributesWantToShow[i];
    //    console.log(i);
    //    console.log(myHashMap[s]);
    //    console.log("name: " + myHashMap[s]["name"]);

    if (myHashMap[s]["name"]=="color" ||
        myHashMap[s]["name"]=="penalty" ||
        myHashMap[s]["name"]=="scaled_color" ||
        myHashMap[s]["name"]=="product_name"){
        continue;
    }

    var inp = document.createElement("INPUT");
    inp.setAttribute("type", "radio");
    inp.setAttribute("name", "left_radio");
    inp.setAttribute("value", myHashMap[s]["name"]);

    var inp2 = document.createElement("INPUT");

    var lab = document.createElement("LABEL");
    var lab2 = document.createElement("LABEL");
    lab.innerHTML = myHashMap[s]["name"];
    lab.setAttribute("class", "btn btn-primary btn-xs");
    lab.setAttribute("id", myHashMap[s]["name"]);
    lab.setAttribute("title", myHashMap[s]["description_en"]);
    lab2.innerHTML = myHashMap[s]["name"];
    lab2.setAttribute("class", "btn btn-primary btn-xs");
    lab2.setAttribute("id", myHashMap[s]["name"]);
    lab.setAttribute("onclick", "change_x(this)");
    lab2.setAttribute("onclick", "change_y(this)");
    lab2.setAttribute("title", myHashMap[s]["description_en"]);

    lab.appendChild(inp);
    inp2.setAttribute("type", "radio");
    inp2.setAttribute("name", "right_radio");
    inp2.setAttribute("value", myHashMap[s]["name"]);
    lab2.appendChild(inp2);

    if(taken==0){
        current_x = myHashMap[s]["name"];
        lab.setAttribute("checked", "checked");
        taken++;
        lab.setAttribute("class", "btn btn-primary btn-xs active");
        
    }else if(taken==1){
        current_y = myHashMap[s]["name"];
        lab2.setAttribute("checked", "checked");
        taken++;
        lab2.setAttribute("class", "btn btn-primary btn-xs active");
    }



    document.getElementById("x_buttons").appendChild(lab);
    document.getElementById("y_buttons").appendChild(lab2);
}
var btn = document.createElement("a");
btn.setAttribute("href","/GraphSettings");
document.getElementById("x_y_columns").appendChild(btn);

// eventListeners
{

    var x_layout_update_linear = {
        xaxis: {
            type: 'linear',
            autorange: true
        }
    };
    var x_layout_update_log = {
        xaxis: {
            type: 'log',
            autorange: true
        }
    };
    var y_layout_update_linear = {
        yaxis: {
            type: 'linear',
            autorange: true
        }
    };
    var y_layout_update_log = {
        yaxis: {
            type: 'log',
            autorange: true
        }
    };

    function main_x_checkbox() {
        if (document.getElementById('main_x_checkbox').checked) {
            main_x_type = "log";
            Plotly.relayout('main', x_layout_update_log);
        } else {
            main_x_type = "linear";
            Plotly.relayout('main', x_layout_update_linear);
        }

    }

    function main_y_checkbox() {
        if (document.getElementById('main_y_checkbox').checked) {
            main_y_type = "log";
            Plotly.relayout('main', y_layout_update_log);
        } else {
            main_y_type = "linear";
            Plotly.relayout('main', y_layout_update_linear);
        }

    }

    function hist_right_x_checkbox() {
        if (document.getElementById('hist_right_x_checkbox').checked) {
            Plotly.relayout('hist_right', x_layout_update_log);
        } else {
            Plotly.relayout('hist_right', x_layout_update_linear);
        }
    }

    function hist_right_y_checkbox() {
        if (document.getElementById('hist_right_y_checkbox').checked) {
            Plotly.relayout('hist_right', y_layout_update_log);
        } else {
            Plotly.relayout('hist_right', y_layout_update_linear);
        }
    }

    function hist_left_x_checkbox() {
        if (document.getElementById('hist_left_x_checkbox').checked) {
            Plotly.relayout('hist_left', x_layout_update_log);
        } else {
            Plotly.relayout('hist_left', x_layout_update_linear);
        }
    }

    function hist_left_y_checkbox() {
        if (document.getElementById('hist_left_y_checkbox').checked) {
            Plotly.relayout('hist_left', y_layout_update_log);
        } else {
            Plotly.relayout('hist_left', y_layout_update_linear);
        }
    }


    function getDataMain(){
        data_main = {
            x: myHashMap[current_x]["data"],
            y: myHashMap[current_y]["data"],
            mode: 'markers',
            type: 'scatter',
            marker: {
                size: 3,
                color: myHashMap["color"]["data"],
                opacity: 0.8
            }
        }
        return data_main;
    }
    function getLayoutMain(){
        layout_main = {
            width: my_width,
            height: my_height,
            title: current_x + ' on ' + current_y,
            xaxis: {
                title: current_x +" ["+ myHashMap[current_x]["units"] + "]",
                type: main_x_type,
                autorange: true
            },
            yaxis: {
                title: current_y  +" ["+ myHashMap[current_y]["units"] + "]",
                type: main_y_type,
                autorange: true
            }
        };
        return layout_main;
    }

    function getDataLeftHist(){
        data_left_hist = {
            x: myHashMap[current_x]["data"],
            type: 'histogram',

        }
        return data_left_hist;
    }

    function getLayoutLeftHist(){
        layout_left_hist = {
            width: my_width,
            height: my_height,
            title: current_x + ' hist',
            xaxis: {
                title: current_x +" ["+ myHashMap[current_x]["units"] + "]",
                type: left_hist_x_type,
                autorange: true
            },
            yaxis: {
                //title: current_y + myHashMap[current_y]["units"],
                type: left_hist_y_type,
                autorange: true
            }
        };
        return layout_left_hist;
    }

    function getDataInfo1(){
        return {
            y: myHashMap[current_x]["data"],
            type: 'box',
            boxpoints: false
        };
    }

    function getLayoutInfo1(){
        return {
            height: my_height,
            title: current_x + ' boxplot',
            yaxis: {
                type: 'log',
            }
        };
    }




    function getDataRightHist(){
        data_right_hist = {
            x: myHashMap[current_y]["data"],
            type: 'histogram',

        }
        return data_right_hist;
    }
    function getLayoutRigthHist(){

        layout_right_hist = {
            //width: my_width,
            height: my_height,
            title: current_y + ' hist',
            xaxis: {
                title: current_y  +" ["+ myHashMap[current_y]["units"] + "]",
                type: right_hist_x_type,
                autorange: true
            },
            yaxis: {
                //title: current_y + myHashMap[current_y]["units"],
                type: rigth_hist_y_type,
                autorange: true
            }
        };
        return layout_right_hist;
    }

    function getDataInfo2(){
        return {
            y: myHashMap[current_y]["data"],
            type: 'box',
            boxpoints: false
        };
    }
    function getLayoutInfo2(){
        return {
            height: my_height,
            title: current_y+' boxplot',
            yaxis: {
                type: 'log',
            },
        };
    }


    function change_x(name) {
        current_x = name.firstChild.data;

        Plotly.newPlot('main', [getDataMain()], getLayoutMain());

        Plotly.newPlot('hist_left', [getDataLeftHist()], getLayoutLeftHist());

        Plotly.newPlot('info', [getDataInfo1()], getLayoutInfo1());
    }

    function change_y(name) {
        current_y = name.firstChild.data;

        Plotly.newPlot('main', [getDataMain()], getLayoutMain());

        Plotly.newPlot('hist_right', [getDataRightHist()], getLayoutRigthHist());

        Plotly.newPlot('info2', [getDataInfo2()], getLayoutInfo2());
    }


    Plotly.newPlot('main', [getDataMain()], getLayoutMain());
    Plotly.newPlot('hist_right', [getDataRightHist()], getLayoutRigthHist());
    Plotly.newPlot('hist_left', [getDataLeftHist()], getLayoutLeftHist());
    Plotly.newPlot('info', [getDataInfo1()], getLayoutInfo1());
    Plotly.newPlot('info2', [getDataInfo2()], getLayoutInfo2());

}
